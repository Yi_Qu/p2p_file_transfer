import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.*;

public class IndexServerThread extends Thread{
    Socket clientSocket;
    String CID;
    
    public IndexServerThread(Socket clientSocket, String CID){
	this.clientSocket = clientSocket;
        this.CID = CID;
    }

    public static void search(ObjectInputStream inStream, ObjectOutputStream outStream){
        try{
            ArrayList<String> returnClients = new ArrayList<String>();
            do{//ask for files
                String serverDownloadMessage = "Which file do you want?";
                outStream.writeUTF(serverDownloadMessage);
                outStream.flush();
                String clientDownloadMessage = inStream.readUTF();
                System.out.println("Client wants " + clientDownloadMessage);
                returnClients = IndexServer.file2Clients.get(clientDownloadMessage);
                if(returnClients != null){
                    serverDownloadMessage = "Client id = " + returnClients + " have the file. Please select one of them.";
                    outStream.writeUTF(serverDownloadMessage);
                    outStream.flush();
                } else {
                    outStream.writeUTF("File not found!");
                }
            } while(returnClients == null);
        } catch(Exception e){
            System.out.println(e);
        }
    }


    public void run(){
	try{
            ObjectInputStream inStream = new ObjectInputStream(clientSocket.getInputStream());
	    ObjectOutputStream outStream = new ObjectOutputStream(clientSocket.getOutputStream());

            //while(true){
            //String serverMessage = "", serverDownloadMessage = "";
            String serverMessage = "";
            //String clientDownloadMessage = "";
            @SuppressWarnings("unchecked")
            ArrayList<String> clientMessage = (ArrayList<String>)inStream.readObject();
            System.out.println(clientMessage);
            if(!(clientMessage.get(0)).equals("-1")){
                System.out.println("Client already registered");
                CID = clientMessage.get(0);
            }
            ArrayList<String> clientFiles = new ArrayList<String>();
            for(String str : clientMessage.subList(1, clientMessage.size())){
                clientFiles.add(str);
            }
            //testing send back file list
	    outStream.writeObject(clientFiles);
	    outStream.flush();
            //update client2Files
	    ConcurrentHashMap<String, ArrayList<String>> currentC2F = IndexServer.client2Files;
	    currentC2F.put(CID, clientFiles);
            IndexServer.client2Files = currentC2F;
            System.out.println("Updated c2f");
            //update file2clients
            ConcurrentHashMap<String, ArrayList<String>> currentF2C = IndexServer.file2Clients;
            for(String fileName : clientFiles){
                System.out.println(fileName);
                //ArrayList<String> fileClients = currentF2C.get(fileName);
                ArrayList<String> fileClients = new ArrayList<String>();
                if(currentF2C.get(fileName) == null){
                    fileClients.add(CID);
                    currentF2C.put(fileName, fileClients);
                }
                fileClients = currentF2C.get(fileName);
                if(!fileClients.contains(CID)){
                    fileClients.add(CID);
                    currentF2C.put(fileName, fileClients);
                    System.out.println("Update f2c");
                }
            }
            System.out.println("Updated f2c");
            
	    //outStream.writeObject(CID);
	    //outStream.flush();
	    System.out.println("Client registration is done for client " + CID);
	    serverMessage ="Client registration is done for client " + CID;
	    outStream.writeUTF(serverMessage);
	    outStream.flush();
            //}
            search(inStream, outStream);
	    //inStream.close();
	    //outStream.close();
	    //clientSocket.close();
	} catch(Exception e) {
	    System.out.println(e);
        }
	      
    }
}
