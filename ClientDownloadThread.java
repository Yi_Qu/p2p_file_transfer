import java.io.File;
import java.io.BufferedReader;
import java.io.BufferedOutputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.net.Socket;


public class ClientDownloadThread extends Thread{
    private static final int BUFFER_SIZE = 1024;
    private Socket requestSocket;
    private String CID;
    private String requestFile;
    
    public ClientDownloadThread(Socket requestSocket, String CID, String requestFile){
        this.requestSocket = requestSocket;
        this.CID = CID;
        this.requestFile = requestFile;
    }

    public void run(){
        try{
            String clientMessage = "", serverMessage = "";
            System.out.println("In request thread");
            ObjectOutputStream clientOut = new ObjectOutputStream(requestSocket.getOutputStream());
            ObjectInputStream clientIn = new ObjectInputStream(requestSocket.getInputStream());
            clientMessage = clientIn.readUTF();//Download from IP
            System.out.println(clientMessage);
            //BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            //serverMessage = br.readLine();//filename
            
            //downloading
            byte[] downloadFile = new byte[BUFFER_SIZE];
            BufferedOutputStream fileStream = new BufferedOutputStream(new FileOutputStream("~/Desktop/test/" + requestFile));
            int currentDownload = 0;
	    while ((currentDownload = clientIn.read(downloadFile, 0, downloadFile.length)) > 0){
		    fileStream.write(downloadFile, 0, currentDownload);
	    }
            serverMessage = "Finish downloading file from " + CID;
            System.out.println(serverMessage);
            clientOut.writeUTF(serverMessage);
            fileStream.close();
        } catch(Exception e){
            System.out.println(e);
        } finally {
            System.out.println("Client to client connection shut off");
            try{
                requestSocket.close();
            } catch(IOException i){
                System.out.println(i);
            }
        }
    }
}
