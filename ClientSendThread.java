import java.io.File;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.*;


public class ClientSendThread extends Thread{
    Socket clientSocket;
    String CID;
    String fileName;
    
    public ClientSendThread(Socket clientSocket, String CID, String fileName){
	this.clientSocket = clientSocket;
        this.CID = CID;
        this.fileName = fileName;
    }
/*
    public static void search(ObjectInputStream inStream, ObjectOutputStream outStream){
        try{
            ArrayList<String> returnClients = new ArrayList<String>();
            do{//ask for files
                String serverDownloadMessage = "Which file do you want?";
                outStream.writeUTF(serverDownloadMessage);
                outStream.flush();
                String clientDownloadMessage = inStream.readUTF();
                returnClients = IndexServer.file2Clients.get(clientDownloadMessage);
                if(returnClients != null){
                    serverDownloadMessage = "Client id = " + returnClients + " have the file. Please select one of them.";
                    outStream.writeUTF(serverDownloadMessage);
                    outStream.flush();
                } else {
                    outStream.writeUTF("File not found!");
                }
            } while(returnClients == null);
        } catch(Exception e){
            System.out.println(e);
        }
    }
*/
    public void run(){
	try{
            ObjectInputStream inStream = new ObjectInputStream(clientSocket.getInputStream());
	    ObjectOutputStream outStream = new ObjectOutputStream(clientSocket.getOutputStream());

            //while(true){
            String serverMessage = "", serverDownloadMessage = "";
            String clientDownloadMessage = inStream.readUTF(); //filename from CID using port X
            String clientIP = clientSocket.getInetAddress().getHostAddress();
            System.out.println("Download from " + clientIP);
            serverDownloadMessage = "Download from " + clientIP;
            outStream.writeUTF(serverDownloadMessage);

            String fileName = clientDownloadMessage.split(" ")[0];
            //int port = Integer.parseInt(clientDownloadMessage.split(" ")[6]);

            File f = new File("~/Desktop/test/" + fileName);
            byte[] fileBytes = new byte[(int)f.length()];
            BufferedInputStream inFileStream = new BufferedInputStream(new FileInputStream(f));
            inFileStream.read(fileBytes, 0, fileBytes.length);
            outStream.write(fileBytes, 0, fileBytes.length);
            outStream.flush();
            inFileStream.close();
            System.out.println("File " + fileName + "downloaded!");
            serverMessage = "File " + fileName + "downloaded!";
            outStream.writeUTF(serverMessage);


	} catch(Exception e) {
	    System.out.println(e);
        }
	      
    }
}














/*
            @SuppressWarnings("unchecked")
            ArrayList<String> clientMessage = (ArrayList<String>)inStream.readObject();
            System.out.println(clientMessage);
            if(!(clientMessage.get(0)).equals("-1")){
                System.out.println("Client already registered");
                CID = clientMessage.get(0);
            }
            ArrayList<String> clientFiles = new ArrayList<String>();
            for(String str : clientMessage.subList(1, clientMessage.size())){
                clientFiles.add(str);
            }
            //testing send back file list
	    outStream.writeObject(clientFiles);
	    outStream.flush();
            //update client2Files
	    ConcurrentHashMap<String, ArrayList<String>> currentC2F = IndexServer.client2Files;
	    currentC2F.put(CID, clientFiles);
            IndexServer.client2Files = currentC2F;
            //update file2clients
            ConcurrentHashMap<String, ArrayList<String>> currentF2C = IndexServer.file2Clients;
            for(String fileName : clientFiles){
                ArrayList<String> fileClients = currentF2C.get(fileName);
                if(!fileClients.contains(CID)){
                    fileClients.add(CID);
                    currentF2C.put(fileName, fileClients);
                    System.out.println("Update f2c");
                }
            }
            
	    //outStream.writeObject(CID);
	    //outStream.flush();
	    System.out.println("Client registration is done for client " + CID);
	    serverMessage ="Client registration is done for client " + CID;
	    outStream.writeUTF(serverMessage);
	    outStream.flush();
            //}
            search(inStream, outStream);
	    //inStream.close();
	    //outStream.close();
	    //clientSocket.close();
*/

