import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.*;
import java.util.*;


public class IndexServer{
    public static ConcurrentHashMap<String, ArrayList<String>> client2Files = new ConcurrentHashMap<String, ArrayList<String>>();
    public static ConcurrentHashMap<String, ArrayList<String>> getClient2Files(){
        return client2Files;
    }
    public static void setClient2Files(ConcurrentHashMap<String, ArrayList<String>> client2Files) {
	IndexServer.client2Files = client2Files;
    }
    public static ConcurrentHashMap<String, ArrayList<String>> file2Clients = new ConcurrentHashMap<String, ArrayList<String>>();
    public static ConcurrentHashMap<String, ArrayList<String>> getFile2Clients(){
        return file2Clients;
    }
    public static void setFile2Clients(ConcurrentHashMap<String, ArrayList<String>> file2Clients) {
        IndexServer.file2Clients = file2Clients;
    }
    
    private static ServerSocket serverSocket = null;

    private static int numClient = 0;
    
    public static void main(String[] argv) throws IOException {
        System.out.println("Start server");
        try{
            while(true){
                numClient++;
                serverSocket = new ServerSocket(5000);
                Socket clientSocket = serverSocket.accept();
                IndexServerThread serverThread = new IndexServerThread(clientSocket, String.valueOf(numClient));
                serverThread.start();
            }
        } catch(IOException i){
            System.out.println(i);
        } finally {
            serverSocket.close();
        }
    }
}
