JC = javac
JR = java
JFLAGS = -g

default: all

all:
	$(JC) *.java

indexServerRun:
	$(JR) IndexServer

clientRun:
	$(JR) Client

clean:
	$(RM) *.class
