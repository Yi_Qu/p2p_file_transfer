import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Client {
    private static final String REPLICATIONLOCATION = "/Users/vincent/Desktop/replications";
    private static final String DOWNLOADLOCATION = "/Users/vincent/Desktop/downloads";
    private static ArrayList<String> fileList = new ArrayList<String>();
    private static ArrayList<String> replicationList = new ArrayList<String>();
    private static String CID = "-1";
    private static final String ServerIP = "208.59.152.28";
    private static final int ServerPort = 5000;
    private static Socket socket2server = null;
    private static ServerSocket downloadServerSocket = null;
    private static Socket socket2client = null;


    public static void main(String[] argv) throws IOException {
        try{
            downloadServerSocket = new ServerSocket(10000);
            System.out.println("Client server listen on port 10000");
        } catch (Exception e){
            System.out.println(e);
        } finally {
            downloadServerSocket.close();
        }
        if(!isRegisted()){
            registration();
        }
        //testing
        System.out.println(IndexServer.getClient2Files());
        System.out.println("Client Start");
    }


    public static void obtain(String fileName, String downloadCID){
        System.out.println("obtain file");
        try{
            Socket downloadClientSocket = downloadServerSocket.accept();
            ClientSendThread clientSend = new ClientSendThread(downloadClientSocket, CID, fileName); 
            clientSend.start();
            
            socket2client = new Socket(downloadClientSocket.getInetAddress().getHostAddress(), 10000);
            ClientDownloadThread clientRequest = new ClientDownloadThread(socket2client, CID, fileName);
            clientRequest.start();
        } catch (Exception e){
            System.out.println(e);
        }
    }

    public static void lookUpFiles(ObjectInputStream objInput, ObjectOutputStream objOutput){
        System.out.println("Request file");
        try{
            String serverMessage = (String)objInput.readObject();
            System.out.println(serverMessage);
            BufferedReader ob = new BufferedReader(new InputStreamReader(System.in));
            String fileName = ob.readLine();
            objOutput.writeObject(fileName);
            objOutput.flush();
            String availableList = (String)objInput.readObject();
            System.out.println(availableList);
            System.out.println("Please select one client from the list shown above.");
            String downloadCID = ob.readLine();
            obtain(fileName, downloadCID);
        } catch(Exception e){
            System.out.println(e);
        }
    }

    public static boolean isRegisted() {
        //File userInfo = new File("./.userInfo");
        //try {
            //BufferedReader reader = new BufferedReader(new FileReader(userInfo));
            //String line = new String(reader.readLine());
	    //CID = line.split(" ")[0];
            //reader.close();
        //} catch (Exception e) {
        //    System.out.println(e);
        //    return false;
        //}
        //return true;
        return false;
    }

    public static void registration() throws IOException {
        /* Register Client in server */
        try {
            ArrayList<String> clientMessage = new ArrayList<String>();
            clientMessage.add(CID);
            String serverMessage = "";
            //String clientMessage = "";
            //clientMessage = clientMessage + CID;
            socket2server = new Socket(ServerIP, ServerPort);
            System.out.println("After creating socket");
            ObjectOutputStream objOutput = new ObjectOutputStream(socket2server.getOutputStream());
            System.out.println("After creating out object");
            
            // This part is for testing. We are adding some dummy files
/*
            File testfile1 = new File("~/Desktop/downloads/test1.txt");
            File testfile2 = new File("~/Desktop/downloads/test2.txt");
            File testfile3 = new File("~/Desktop/downloads/test3.txt");
            File testfile4 = new File("~/Desktop/downloads/test4.txt");

            File testfile5 = new File("~/Desktop/replications/test1.txt");
            File testfile6 = new File("~/Desktop/replications/test2.txt");
            File testfile7 = new File("~/Desktop/replications/test3.txt");
            System.out.println("After creating files");
*/
            // File[] downloadFiles = downloadFolder.listFiles();
            //ArrayList<File> downloadFiles = new ArrayList<File>();
            //downloadFiles.add(testfile1);
            //downloadFiles.add(testfile2);
            //downloadFiles.add(testfile3);
            //downloadFiles.add(testfile4);
            
            File downloadFolder = new File(DOWNLOADLOCATION);
            File[] downloadFiles = downloadFolder.listFiles();
            System.out.println("After adding files");
            for (File f : downloadFiles) {
                System.out.println("We are inside the for loop");
                if (f.isFile()) {
                    fileList.add(f.getName());
                    clientMessage.add(f.getName());
                    //System.out.println(f.getName());
                }
            }
            /* get replication names */
            File replicationFolder = new File(REPLICATIONLOCATION); 
            File[] replicationFiles = replicationFolder.listFiles(); 
            for(File f : replicationFiles){
                if(f.isFile()){
                    replicationList.add(f.getName());
                    clientMessage.add(f.getName());
                }
            }
            
            System.out.println("Files in my downloads");
            //String downloadList = Arrays.toString(fileList.toArray());
            //String repList = Arrays.toString(replicationList.toArray());
            //clientMessage = clientMessage + " " + downloadList;
            //clientMessage = clientMessage + " " + downloadList;
            //System.out.println(downloadList);
            //System.out.println(repList);

            /* Write to server */
            objOutput.writeObject(clientMessage);
            System.out.println("We have written the list");
            objOutput.flush();
            System.out.println("After flush");

            /* Get from server */
            ObjectInputStream objInput = new ObjectInputStream(socket2server.getInputStream());
            System.out.println("After creating an input obj");
            //testing
            Object filesReturnedBack = objInput.readObject();
            System.out.println("We got this data back" + filesReturnedBack);

            System.out.println("After creating Input objects");
            
            serverMessage = objInput.readUTF();
            System.out.println(serverMessage);
            if(!CID.equals("-1")){
                CID = serverMessage.split(" ")[6];
                System.out.println("Clint ID is updated to " + CID);
            }

            /* Close connection */
            //objOutput.close();
            //objInput.close();
            //socket2server.close();
            
            //while(true){ // Assume download only once
                lookUpFiles(objInput, objOutput);
            //}

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
